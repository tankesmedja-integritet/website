---
title: Windows
---

## Windows
---

På senare tid har Microsoft börjat inkludera mer och mer övervakningsfunktioner i Windows. Till stor del är de svåra att välja bort, så Windowsanvändare bör allvarligt överväga att byta operativsystem.

För vissa är det dock av olika anledningar inte ett alternativ att byta, i så fall kan man göra följande för att försöka begränsa skadan:

- Försök att [använda operativsystemet utan Microsoft-konto](https://www.reddit.com/r/Windows11/comments/tvdaxl/psa_you_can_set_up_windows_11_without_a_microsoft/)
- Använd verktyg, t.ex. [SophiApp](https://github.com/Sophia-Community/SophiApp)
eller [Privacy.sexy](https://privacy.sexy/) för att stänga av diverse spionfunktioner.

