---
title: Sviter
---

## Sviter

> En mjukvarusvit är en samling tjänster som ett företag tillhandahåller, ofta är de sammankopplade för att fungera bättre med varandra än med andra alternativ.

Rekommenderade
 - [Proton](./proton)
 - FramaSoft

Populära
 - [Google Workspace](./google)
 - [Microsoft 365](./microsoft)
 - [iCloud](./apple)
