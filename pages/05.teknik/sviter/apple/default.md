---
title: Apple
---

[![](https://shields.tosdr.org/en_158.svg)](https://tosdr.org/en/service/158)

## iCloud
---

Navet i Apples nätverk av online-tjänster är iCloud, som förutom vanlig filförvaring också är backend till deras e-posttjänst, deras bildhantering, m.m.

Den vanliga versionen har väldigt svagt skydd, men om användaren väljer til [Advanced Data Protection](https://support.apple.com/en-us/HT202303#advanced) blir den mesta datan bättre skyddad, men fortfarande inte allt. Mail, kontakter och kalender förblir tillgängliga för Apple att läsa igenom. Dock krypteras iCloud Drive.

## Streaming och appar
---

Apple har ett antal streamingtjänster, däribland Apple Music, Apple TV, och har en digital mjukvarubutik, App Store.


