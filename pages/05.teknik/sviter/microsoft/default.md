---
title: Microsoft
---

## Microsoft-tjänster
---

En kombination av Microsofts olika applikationer. Ofta ingår det en licens när man köper en dator med Windows förinstallerat, eller så köps det licenspaket av arbetsgivare, skolor, och liknande. 
Microsoft är amerikanska, och har sin data på amerikanska servrar, med allt det innebär.

[![](https://shields.tosdr.org/en_244.svg)](https://tosdr.org/en/service/244)

#### E-post
Microsofts eposttjänst heter Outlook. I likhet med t.ex. Google lagras informationen okrypterat, men de lovar åtminstone i sin integritetspolicy att inte använda datan för att sälja annonser till sina användare:
> Microsoft does not use what you say in email, human-to-human chat, video calls or voice mail, or your documents, photos, or other personal files to target ads to you.
(Aktuell 26/08/2023)

#### Kalender
Microsoft har en kalendertjänst, Outlook Calendar.

#### Moln
Microsoft erbjuder två molntjänster, Sharepoint för delade filer, och OneDrive för personliga filer.

