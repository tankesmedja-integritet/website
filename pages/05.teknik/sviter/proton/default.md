---
title: Proton
---

## Proton
---

Proton-sviten fokuserar på att skydda användarens integritet. Du kan skaffa ett konto utan att uppge någon personligt identifierbar information alls, och gratisversionen är en duglig lösning för modest användning, och vill man uppgradera kan man förutom kortbetalning, payppal eller banköverföring även betala med Bitcoin eller kontanter i ett kuvert.

All data som Proton hanterar skyddas av zero-access-kryptering, vilket innebär att enbart användaren kan se innehållet i filerna, som är krypterade med nycklar som Proton inte har, utan återskapas varje gång du loggar in.

#### E-post
Protonmail har stöd för end-to-end-kryptering i form av OpenPGP, och för betalande användare tillåter egna domäner för epostaddresser. I övrigt har den alla de fördelar som hela sviten delar, som zero-access kryptering, och privata betalningsalternativ.

#### Kalender
Proton Calendar har alla de fördelar som hela sviten delar, som zero-access kryptering, och privata betalningsalternativ.

#### Moln
Protons monltjänst, Proton Drive, har alla de fördelar som hela sviten delar, som zero-access kryptering, och privata betalningsalternativ.

#### VPN
Proton VPN har alla de fördelar som hela sviten delar, som zero-access kryptering, och privata betalningsalternativ.

