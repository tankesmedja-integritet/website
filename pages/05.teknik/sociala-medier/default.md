---
title: Sociala medier
---

## Sociala Medier
De flesta sociala medier har [övervakning som affärsmodell](../oevervakning-som-affaersmodell), genom att veta så mycket som möjligt om sina användare kan de ta högre per-annons-priser av annonsörerna, genom att minimera mängden bortkastade annonsplaceringar.

> *"Half the money I spend on advertising is wasted, and the trouble is I don't know which half."* - John Wanamaker

Hur väl detta ens fungerar [går att diskutera](https://onezero.medium.com/how-to-destroy-surveillance-capitalism-8135e6744d59), men så länge det används för att rättfärdiga storskalig datainsamling blir effekten densamma. Tumregeln är, och bör alltid vara: sociala medier är en publik plats, och spridningen av allt du lägger upp är bortom din kontroll för alltid.

Dock går det att göra vissa saker för att begränsa skadan. 
Vissa saker går att göra för alla sociala medier: om du använder en appversion på din smartphone kan du neka tillgång till så många "permissions" du kan via telefonens inställningar, eller också använda tjänsten genom telefonens webbläsare.

### Mastodon
Vår rekommenderade sociala medietjänst. Mastodon bygger inte på att ett gigantiskt företag behöver vara lönsamt för att forstätta fungera, och behöver därför inte heller finansieras genom att övervaka sina användare. 
Dock bör man fortfarande iaktta tumregeln att allt som publiceras är att betrakta som offentligt, eftersom varken tutningar eller direktmeddelanden är krypterade. För privat kommunikation rekommenderas i stället en bra [chatt-tjänst](../chattar)

### Facebook
Facebook är en av de värsta när det kommer till att samla in information om användare.

### Instagram

### Tiktok

### X
X (f.d. Twitter) har samma grundläggande problem som de flesta andra sociala medier, eftersom deras annonsbaserade affärsmodell bygger på övervakning.
För att minimera skadan kan du ändra inställningarna under "Privacy and Safety" -> "Data sharing and personalization". Gå igenom dem, och stäng av allt som är valbart, och radera information de redan fått om dig.

### Snapchat
Snapchat byggde sin tjänst på funktionen att bilder försvinner efter de skickats, något som framstår som väldigt positivt ur integritetssynpunkt. 
Detta har visat sig vara ganska tomma löften. Inte bara har Snap Inc. visat sig spara mycket data om sina användare, om mottagaren av meddelandet använt opålitliga tredjepartsklienter har det dessutom förekommit att massor av [meddelanden läckt](https://www.pcmag.com/news/confirmed-snapsaved-hack-led-to-snapchat-photo-leak)

### Discord


