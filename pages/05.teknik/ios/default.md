---
title: iOS
---

## iOS
---

iOS är ganska bra ur integritetssynpunkt, om man valt rätt inställningar. En inställning som gör mycket skillnad är [låst läge](https://support.apple.com/sv-se/HT212650), men den kan vara lite väl extrem för många. Om man tycker det är alltför obekvämt att inte ha tillgång till alla de funktioner man förlorar finns det andra inställningar att titta på.

Som apple-användare är man oftast användare av en hel mjukvarusvit från Apple, och i den [övergripande artikeln om Apple-mjukvara](../sviter/apple) finns rekommendationer om inställningar man bör välja, var särskilt uppmärksam på inställningarna om iCloud.

Apple har även en [egen guide](https://support.apple.com/sv-se/guide/iphone/iph6e7d349d1/ios) till integritetsskydd på iPhone, men de råden handlar om att skydda sig från andra aktörer, många gånger genom att tillåta Apple själva att samla in *mer* information.

Om du inte väljer att använda Advanced Data Protection, bör du ändå gå in i dina systeminställningar och ändra vissa saker. Särskilt intressant är *Integritet och säkerhet &rarr; Spårning*, där du helt enkelt kan stänga av rätten för vissa appar att spåra dig, plus en knapp högst upp som automatiskt nekar alla förfågningar i framtiden.

*Integritet och säkerhet &rarr; Platstjänster &rarr; Systemtjänster* (längst ner), där kan du välja bort många ändamål för Apples platsdaatainsamling som "förbättringar". I detta området finns ytterligare inställningar att titta på för de appar du har installerade.

Under *Integritet och säkerhet* finns också *Analys och förbättringar* samt *Apple-reklam*, där du bör välja bort all övervakning du kan.
