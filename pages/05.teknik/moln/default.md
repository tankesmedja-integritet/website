---
title: Moln
---

## Moln
---
Molntjänster är bland de dyraste att driva, eftersom varje användare använder serverresurser, nämligen lagringsutrymme, även när de inte aktivt använder tjänsten. Därför finns få gratislösningar, särskilt om man inte är okej med att tjänsteleverantören tjänar sina pengar på ens data

#### [Google Drive](../sviter/google#Moln)
Google Drive är ett populärt alternativ, men informationen lagras okrypterat, och de tar sig rätten att både läsa igenom ens data, och dela den med tredje part. Detta innebär att det inte finns något integritetsbeskyddande sett att använda tjänsten.

#### [Microsoft Onedrive](../sviter/microsoft#Moln)
Onedrive är förinstallerat på alla Windows-datorer, vilket troligen är anledningen till att de har särskilt många användare. Även om Microsft skriver i sin integritetspolicy att de inte läser ens faktiska data, så gör de det med [metadata](../begrepp/metadata). Onedrive är dessutom okrypterat, vilket innebär att det inte finns något integritetsbeskyddande sett att använda tjänsten.

#### [Apple iCloud](../sviter/apple#Moln)
Apple iCloud är den svagaste länken i Apples annars relativt bra ekosystem. Detta eftersom de flesta apple-enheter använder iCloud för backups. T.ex. är iMessage-meddelanden [fullsträckskrypterade](../begrepp/kryptering#Fullsträckskryptering,_aka_end-to-end), men säkerhetskopiorna som laddas upp på iCloud är okrypterade, så om man använder det är de ändå okrypterade. Vissa appar krypterar sina backups *innan* de laddas upp, och i vissa andra går det att [ställa in](../ios).

#### [Proton](../sviter/proton#Moln)
Proton-sviten är integritetsfokuserad, och deras molntjänst Proton Drive är inget undantag. Det finns en kostnadsfri variant som ger 1 GB lagringsutrymme, och betalda varianter med 500 GB eller 3 TB.

Information som lagras på Proton Drive är [fullsträckskrypterade](../begrepp/kryptering#Fullsträckskryptering,_aka_end-to-end)

#### Cryptpad

#### Nextcloud

#### OnlyOffice

