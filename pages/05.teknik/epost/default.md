---
title: E-post
---

# E-post


Det går visserligen att använda sin egen e-postserver, vilket är ett bra alternativ för den som har den tekniska kunnigheten och resuserna, men för de allra flesta är det enda alternativet att använda en mejltjänst som tillhandahålls av någon annan.
E-post är ett gammalt system, som kräver att metadata skickas okrypterat, vilket gör det tekniskt omöjligt att erbjuda samma säkerhetsgarantier som t.ex. Signal gör för sina meddelanden.

## Rekommenderade
### [Protonmail](../sviter/proton)
 - [Zero-access-kryptering](../begrepp/kryptering)
 - Inbyggd [PGP-kryptering](../begrepp/kryptering)
 - Kräver ingen persondata för att skapa konto
 - God funktionalitet och användarupplevelse
### Mailbox.org
 - Inbyggd [PGP-kryptering](../begrepp/kryptering)
 - Kräver ingen persondata för att skapa konto
 - God funktionalitet och användarupplevelse
### Tutanota
 - [Zero-acces-kryptering](../begrepp/kryptering)
 - Kräver ingen persondata för att skapa konto
 - God funktionalitet och användarupplevelse
### Startmail
 - [Zero-acces-kryptering](../begrepp/kryptering)
 - Kräver ingen persondata för att skapa konto
 - God funktionalitet och användarupplevelse

## Populära, men ej rekommenderade
### [Gmail](../sviter/google)
Gmail använder innehållet i dina mail till allt från marknadsföring till att träna AI-modeller på.
### [Microsoft Outlook](../sviter/microsoft)

[Privacy Guides om e-post](https://www.privacyguides.org/en/email/)
[Privacy Tools om e-post](https://www.privacytools.io/privacy-email)
