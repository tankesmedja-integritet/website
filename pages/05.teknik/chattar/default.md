---
title: Chattar
---

## Chattar
---
Chattar är samtidens vanligaste sätt att kommunicera, och även om många leverantörer skryter med sin kryptering är det ofta ganska meningslöst.

#### Signal
End-to-end-krypterat, och sparar ingen metadata. Dock kräver tjänsten att man använder ens riktiga telefonnummer, vilket kan vara ett problem för vissa.
Om denna sajten skulle innehålla bara ett tips, skulle det vara: Använd Signal! Appen kombinerar förstklassig kryptering, öppenhet och användarvänlighet samtidigt som den är gratis.
Det är inte konstigt att den toppar listan på alla seriösa rekommendationer av chat-appar.

#### Whatsapp
Krypterat end-to-end by default, men sparar metadata, [vilket bör ses som en brist](../begrepp/metadata). Och ägs av Facebooks moderbolag Meta, vilket är en röd flagga för många. Dessutom proprietär, så det går inte att oberoende bekräfta att den är end-to-end-krypterad. Bättre än många av alternativen, men har fortfarande stora brister.

#### iMessage
Apple's iMessage är krypterat till andra iPhones, men inte till övriga telefoner. Dessutom är backups okrypterade, så om du har iCloud-backups kan Apple läsa dina meddelanden. De använder en egen krypteringsmetod, till skillnad från det öppna protokollet som utvecklades av Signal och används av bland andra Whatsapp, Facebook Messenger och Google Messages.

#### Telegram
Skryter med att de är krypterade, men den krypteringen är bara ett valbart läge, vilket innebär att den mesta kommunikationen inte är end-to-end-krypterad. Dessutom är även Telegrams kryptering en icke-standardiserad lösning, som tidigare visats [innehålla buggar.](https://ethz.ch/en/news-and-events/eth-news/news/2021/07/four-cryptographic-vulnerabilities-in-telegram.html)

#### Facebook Messenger
Skryter med att de är krypterade, men den krypteringen är bara ett valbart läge, vilket innebär att den mesta kommunikationen inte är end-to-end-krypterad. Dessutom samlas stora mängder metadata in och knyts dessutom till ditt Facebook-konto.

- [Washington Posts jämförelse](https://www.washingtonpost.com/technology/2023/08/22/encryption-imessage-whatsapp-google/)
- [Mozillas jämförelser på Privacy Not Included](https://foundation.mozilla.org/en/privacynotincluded/categories/video-call-apps/)
- [Privacy Guides rekommendationer](https://www.privacyguides.org/en/real-time-communication)
- [Privacy Tools rekommendationer](https://www.privacytools.io/privacy-messaging)
