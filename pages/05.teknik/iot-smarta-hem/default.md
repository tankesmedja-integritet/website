---
title: IoT/Smarta hem
---

IoT står för *Internet of Things*, och innebär att föremål som inte traditionellt
varit datorer blir internetuppkopplade, vilket bland annat innefattar så kallade
smarta hem, samlingsnamnet för uppkopplade saker som vitvaror, TV-apparater
och belysning.

IoT innefattar även saker utanför hemmet, som bilar eller smarta städer.
De allra flesta IoT-föremål tar väldigt lite hänsyn till konsumentens integritet,
och en enkel tumregel är att välja bort uppkopplade alternativ.

Om du vet att du definitivt vill välja en smart apparat, eller om det helt enkelt
inte finns ett realistiskt alternativ i produktkategorin, finns nedan några
hjälpsamma externa guider:

[Privacy Not Included från Mozilla](https://foundation.mozilla.org/en/privacynotincluded/)
har lättförstådda recensioner för många olika produktklasser, och bör vara det
första stället att leta information på, i synnerhet kategorierna
[Smart Home](https://foundation.mozilla.org/en/privacynotincluded/categories/smart-home/)
och
[Cars](https://foundation.mozilla.org/en/privacynotincluded/categories/cars/)

Det finns även en del artiklar på ämnet, med specifika råd:

[Your Smart TV Knows What You’re Watching - The Markup](https://themarkup.org/privacy/2023/12/12/your-smart-tv-knows-what-youre-watching)
