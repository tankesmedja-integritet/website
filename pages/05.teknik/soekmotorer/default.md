---
title: Sökmotorer
---

## Sökmotorer
---
Att kunna använda flera sökmotorer är alltid bra, eftersom de ofta ger märkbart olika resultat för samma söktermer. Det finns dock anledning att göra vissa avväganden när du väljer vilken sökmotor du vill använda.

Så småningom kommer de flesta, om inte alla, sökmotorerna få egna sidor.

## Rekommenderade alternativ

### DuckDuckGo
[DuckDuckGo](https://duckduckgo.com) är den mest etablerade integritetsrespekterande sökmotorn. Den har ett eget index, och levererar textsökresultat från denna. För video- och bildresultat förlitar den sig på Bings API, men anonymiserar sökningen innan den skickas vidare dit. DuckDuckGo finansieras via [kontextbaserade annonser](../annonstyper)

### Startpage
[Startpage](https://startpage.com) är i princip ett skal för Google-sökningar, den tar emot och anonymiserar sökningen innan den skickas vidare till Google. Det är ett bra alternativ för den som tycker att Googles kvalitet är oslagbar. Startpage finansieras precis som DuckDuckGo av [kontextbaserade annonser](..annonstyper).

### Brave Search
[Brave Search](https://search.brave.com) är den inbyggda sökmotorn i webbläsaren [Brave](../webblaesare/brave), men den går att använda i andra webbläsare. Den använder ett eget index, och lyfter fram funktioner som AI-baserade sammanfattningar av sökresultat och "Goggles", som tillåter användaren att påverka hur sökmotorn prioriterar bland sökresultaten.

### SearXNG
[SearXNG](https://searxng.org) är inte en traiditionell sökmotortjänst, utan en öppen mjukvara som samlar in sökningar och kan sammanställa sökresultat från flera andra källor. Eftersom SearXNG är decentraliserat varierar finansieringsmodeller m.m. mellan olika instanser. För en lista över offentliga instanser, se [searx.space](https://searx.space/)

### Mullvad Leta
[Mullvad Leta](https://leta.mullvad.net) är en betald sökmotor från det svenska VPN-företaget [Mullvad](../vpn/mullvad), likt Startpage är det en front-end för Google-resultat, men är exklusiv till Mullvads betalande kunder.

## Ej rekommenderade alternativ

### Google
Den överlägset mest använda sökmotorn i världen är också flaggskeppsprodukten för det största övervakningsföretaget i världen. Google är pionjärerna inom [övervakning som affärsmodell](../begrepp/oevervakning-som-affaersmodell)

### Bing
Microsofts sökmotor är inte mycket bättre än Google ur integritetssynpunkt.

### Yahoo!
Yahoo! brukade vara en populär sökmotor, men har med tiden tappat sin relevans.

### Baidu
Baidu är en populär kinesisk sökmotor.

### Yandex
Yandex är en populär rysk sökmotor.

