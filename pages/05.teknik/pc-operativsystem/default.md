---
title: PC-operativsystem
---

## PC-operativsystem
---

### Windows 
 - Överväg starkt att inte använda Windows över huvud taget
 - Installera om det, om det kom med datorn
 - Om W10 eller tidigare: installera utan Microsoft-konto
 - Stäng av telemetri
 - Avinstallera bloatware

### Mac 
 - Undvik iCloud
 - Du blir apple-användare

### Linux 
 - Välj en distro du gillar
 - Secure boot
 - Diskkryptering
 - Undvik WINE
 - MAC-address-randomisering

