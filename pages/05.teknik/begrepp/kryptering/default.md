---
title: Kryptering
---

## Kryptering
---

#### Symmetrisk kryptering
Den enklaste sortens kryptering är att båda personerna har en och samma *nyckel*, ett hemligt tal, som genom någon algoritm kan omvandla mellan ett okrypterat meddelande och dess krypterade form, som för någon utomstående ser ut som slumpmässiga tecken.

#### Asymmetrisk kryptering
Asymmetrisk kryptering innebär att varje person har ett *nyckelpar*, en privat nyckel som bara den personen vet, och en publik nyckel som publiceras öppet. De använder algoritmer som är så fiffiga att det som krypterats med den ena nyckeln kan bara avkrypteras av den andra.

Det möjliggör två saker: Dels att skapa krypterade meddelanden som bara kan läsas av en annan person genom att de använder sin hemliga nyckel. Man kan också genom att kryptera något med ens egen privata nyckel "signera" det, så att vem som helst kan avkryptera det, men då också kan känna sig säkra på att rätt avsändare faktiskt skickat det.

Oftast slipper du ha koll på dina nycklar själv, det sköts bakom kulisserna och kan genereras slumpmässigt eller baserat på t.ex. ditt användarnamn och lösenord om det är viktigt att de är samma från session till session.

#### HTTPS/TLS
HTTPS är webbsidor krypterade med Transport Layer Security. Det är det som ger dig "hänglåset i addressfältet". Det är en kombination av de ovanstående koncepten. Det första som händer är att en Certificate Authority, någon trovärdig aktör, använder signeringsmetoderna från asymmetrisk kryptering för att gå i god för att sidan är den som den utger sig för att vara. Servern skickar den informationen tillbaka till klienten, som skapar symmetrisk nyckel och skickar tillbaka den till servern, krypterad med serverns publika nyckel. Därefter används symmetrisk kryptering.

#### Fullsträckskryptering, aka end-to-end
Fullsträckskryptering innebär att meddelandet krypteras så att servern som förmedlar meddelandet inte kan avkryptera det. Antingen genom att det finns en symmetrisk nyckel som bara användarna vet, eller att användarna har egna privata/publika nyckelpar.

#### [Zero access](https://proton.me/blog/zero-access-encryption)
I vissa fall, som om en server mottar ett okrypterat e-postmeddelande, finns det inget bra sätt att implementera fullsträckskryptering. I de fallet är det näst bästa när servern väl tagit emot ett sådant okrypterat meddelande direkt kryptera det med mottagarens publika nyckel innan det ens sparas på servern. Då kan bara den avsedda mottagaren läsa meddelandet.

#### OpenPGP
OpenPGP är den vanligaste standarden för asymmetrisk kryptering. Den används ofta i sammanhang där man vill skicka ett krypterat meddelanden över protokoll som annars inte stödjer kryptering, t.ex. e-post.


[EFF:s surveillance self defense](https://ssd.eff.org/module/key-concepts-encryption)

