---
title: Annonser
---

## Annonser
---

#### Riktade annonser
Riktade annonser är sådana som väljs ut baserat på vem den tilltänkta mottagaren är. Exempel på dessa är direktreklam i brevlådan till alla som tar studenten, reklamsnutten som spelas upp innan ett youtube-klipp, eller en annons som dyker upp på webben baserat på saker du sökt på tidigare.

Riktade annonser är beroende av att veta så mycket som möjligt, därför kommer de företag som förlitar sig på riktade annonser naturligt dras mot [en affärsmodell som bygger på övervakning](../oevervakning-som-affaersmodell)

#### Kontextbaserade annonser
Annonser som placeras baserat på sammanhanget de är i. Exempel på dessa är annonser från lokala företag i den lokala tidningen, Youtube-personligheter som personligen gör reklam för VPN-företag som del av klippet, eller en annons som dyker upp högst upp på en sida av sökresultat, baserat på vad du sökt på.

Kontextbaserade annonser placeras utan vetskap om vem som kommer se dem, utan bara i vilket sammanhang de kommer synas.
