---
title: Metadata
---

## Metadata
---
> We kill people based on metadata.
>
> -- <cite>Michael Hayden, tidigare chef för NSA och CIA</cite>

#### Vad är metadata?
Metadata är data om data. I t.ex. ett chattmeddelande är metadata inte själva texten som skickas utan all annan information *om* meddelandet. Vem som skickat meddelandet, till vem, när, från vilken plats, o.s.v.
Många tjänster stoltserar med att inte analysera själva datan, utan "bara" metadata.

#### Varför bry sig?
Metadatan för en användare, eller ett meddelande, är inte särskilt farligt. Men när ett företag samlar in metadata från fler och fler håll går det snabbt att konstruera en talande bild. Om jag vet att du skickade fjorton meddelanden till ditt ex mellan klockan ett och tre i söndags morse behöver jag inte läsa meddelanden för att kunna förstå mig på situationen.

Om jag har metadata från både dig, och de du kommunicerar med, och alla som de i sin tur kommunicerar med, går det dessutom att bygga upp ett kontaktnät - en så kallad *social graph* - som gör att den lilla information jag har plötsligt kan generaliseras brett.

Om imperiet i Star Wars kunde veta vem både Leia och Obi-Wan hade kraft-kommunicerat med, hade det varit lätt att hitta Luke, Yoda, och hela rebellalliansen genom att titta på gemensamma nämnare. Då kanske det bara blivit en Star Wars-film i stället för nio.

[EFF: Why metadata matters](https://ssd.eff.org/module/why-metadata-matters)
