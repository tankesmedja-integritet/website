---
title: Spårning
---

## Airtags
Bluetooth-spårare som Apples AirTag eller Samsung SmartTags kan användas för 
att spåra vad de än fästs vid. Tanken är att de ska kunna hjälpa den som är 
orolig för att tappa bort föremål som nycklar eller plånböcker, men de används
ofta för stalking.

Apples Airtags kan kännas igen av både Android- och iPhone-telefoner, och andra
märken av spårare kan upptäckas med hjälp av t.ex. Android-appen
[AirGuard](https://github.com/seemoo-lab/AirGuard) från TU Darmstadt.

För mer läsning på ämnet:
(Electronic Frontier Foundation Surveillance Self-Defense - How to: Detect Bluetooth Trackers)[https://ssd.eff.org/module/how-to-detect-bluetooth-trackers]

## Stalkerware
Stalkerware är ett öknamn på mjukvara som används för att spåra någon annans 
telefon eller dator. Dessa marknadsförs ofta som verktyg för föräldrar att hålla
koll på sina barn, men är ibland mer rakt på sak med att de används för att
övervaka sin partner.

Det enklaste sättet att undvika att någon installerar sådant är att inte låta
*någon* få tillgång till din telefon/dator, genom att välja en svårgissad kod,
och sedan inte dela ut den.

Sådan mjukvara är designad för att vara svår att upptäcka, så det är ofta en
fråga om att gå på känsla: Verkar din misstänkta stalker ha orimligt bra koll 
på var du befinner dig och vad du gör, till exempel?
[Coalition Against Stalkerware](https://stopstalkerware.org/information-for-survivors/)
