---
title: Domännamnservrar (DNS)
---

## Domännamnservrar (DNS)

Domännamnservrar är som internets telefonkataloger.

Några DNS-leverantörer som anses allmänt trovärdiga är:

- [Adguard](https://adguard-dns.io/en/welcome.html)
- [Cloudflare](https://www.cloudflare.com/application-services/products/dns/)
- [NextDNS](https://nextdns.io/)
- [Quad9](https://www.quad9.net/)

Läs mer:
- [Domännamn. Allt du vill veta om din adress på nätet](https://internetstiftelsen.se/kunskap/rapporter-och-guider/domannamn/)
- [DNS Resolvers - Privacyguides](https://www.privacyguides.org/en/dns/)
