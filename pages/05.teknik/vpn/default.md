---
title:VPN
---

# Virtuella Privata Nätverk
---

VPN:er skickar din trafik till sina servrar, varifrån den sedan skickas vidare till sin slutdestination. Detta gör att trafiken ser ut att komma från företagets servrar istället för från din dator.

Detta kan vara användbart i vissa situationer, men för de allra flesta är IP-addressen långt ifrån den svagaste länken, och kommer därför inte kunna hjälpa till. Om din webbläsare eller ditt operativsystem avslöjar dig, eller till och med aktivt skvallrar om din aktivitet, tjänar du inget på att använda en VPN.

[En VPN kan dock vara användbar](https://www.doineedavpn.com/) om du inte litar på din internetleverantör, och din internetkommunikation inte är SSL-krypterad. Då kan du förflytta din tillit till VPN-företaget, som i vissa fall är mer pålitligt.

Om du är säker på att du behöver en VPN är dessa tjänster några av dem som har högst anseende bland integritets folk som sysslar med integritetfrågor:

- Mullvad
- IVPN
- ProtonVPN

[Do I need a VPN? från IVPN](https://www.doineedavpn.com/)
[Electronic Frontier Foundation om VPN:er](https://ssd.eff.org/module/what-should-i-know-about-encryption)
[Freedom of the press foundations VPN-guide](https://freedom.press/training/choosing-a-vpn/)
[Privacyguides rekommenderade VPN-tjänster](https://www.privacyguides.org/en/vpn/)
[Privacytools rekommenderade VPN-tjänster](https://www.privacytools.io/privacy-vpn)
