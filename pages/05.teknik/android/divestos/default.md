---
title: DivestOS
---

## DivestOS
---
DivestOS är baserat på LineageOS, men lägger till funktioner som låst bootloader för ökad säkerhet. Likt LineageOS tar DivestOS bort det mesta av Googles proprietära kod, utan att ersätta det med något annat. Detta leder till att många appar inte kommer fungera om man inte installerar Googles Android-appar eller ett alternativ till dessa. 

DivestOS har nästan lika brett hårdvarustöd som LineageOS, och fungerar på de flesta android-telefoner.

DivestOS saknar kompatibilitetsåtgärder, så många appar som förlitar sig på Google Play Services kommer inte fungera som vanligt.

Sammanfattning:
- **Stödjer** verified boot
- Har **ingen kompatibilitet** med appar som förlitar sig på Google Play Services
