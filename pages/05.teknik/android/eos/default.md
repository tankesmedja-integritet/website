---
title: /e/OS
---

## /e/OS
---

/e/OS är ett försök att göra av-googlade telefoner mer tillängliga för icke-tekniska användare. Den går att köpa förinstallerad på telefoner av märket [Murena](https://murena.com).

Sammanfattning:
- Stödjer verified boot på [**vissa**](https://doc.e.foundation/support-topics/support#does-eos-allow-for-the-bootloader-to-be-locked-on-phones-that-support-verified-boot) enheter
- Använder MicroG för kompatibilitet med appar som kräver Google Play Services

