---
title: Iodé OS
---

## Iodé OS
---

#### [iodé](./iode)
Iodé är ett försök att göra av-googlade telefoner mer tillängliga för icke-tekniska användare.

- Stödjer verified boot för **vissa** enheter
- Använder MicroG för kompatibilitet med appar som kräver Google Play Services

