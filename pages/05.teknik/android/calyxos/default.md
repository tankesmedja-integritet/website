---
title: CalyxOS
---

## CalyxOS
---

CalyxOS tar bort alla spårningsfunktioner i Android, och förstärker operativsystemets integritetsskydd och säkerhetsmodell utan att offra "vanlig" funktionalitet i så stor utsträckning som möjligt.

För app-kompatibilitet använder operativsystemet MicroG, en alternativ implementation av Google Play Services som kör i ett läge med extra privligier i operativsystemet. Detta [kan enligt vissa](https://blog.privacyguides.org/2022/04/21/grapheneos-or-calyxos/#sandboxed-google-play-vs-privileged-microg) utöra en säkerhetsrisk, å andra sidan kan du undvika Google i mycket större utsträckning, och kan t.ex. välja en annan leverantör för platstjänster.

Sammanfattning:
- **Stödjer** verified boot
- Använder **MicroG** för kompatibilitet
