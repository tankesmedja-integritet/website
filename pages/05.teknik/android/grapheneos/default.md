---
title: GrapheneOS
---

## GrapheneOS
---
GrapheneOS är kompromisslöst integritetsfokuserat, och har många funktioner som finns till för att stärka användarens integritet och säkerhet.

För kompatibilitet med Google-appar använder operativsystemet vad det kallar för "sandboxed Google play"
```TODO: fyll på```

- **Stödjer** verified boot
- Använder officiella Google Play Services (med vissa begränsningar) för kompatibilitet med appar som kräver Google Play Services
