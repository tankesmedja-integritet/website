---
title: Android
---

## Android
---
Samtliga av dessa alternativ är väldigt bra ur integritetssynpunkt, det som avgör bör vara om din befintliga telefon är kompatibel, och avvägningar mellan säkerhet, kompatibilitet med appar som kräver Google Play Services, och hur mycket av Googles mjukvara du står ut med.

- [CalyxOS](./calyxos)
- [GrapheneOS](./grapheneos)
- [/e/OS](./eos)
- [iodé](./iode)
- [DivestOS](./divestos)
- [lineageos](./lineageos)

##### Google Play Services eller MicroG
De flesta komponenterna av Android som operativsystem är Open Source, så det är relativt lätt att göra en egen version av det, något som de flesta telefontillverkare ju också gör. Ett viktigt undantag är *Google Play Services*, vars funktioner många andra appar bygger på. Detta inkluderar bland annat platstjänster, möjligheten att motta pushnotiser, och förstås Google Play Store, som sköter in-app-betalningar och nedladdning av nya appar.

Många av de operativsystem vi rekommenderar förlitar sig i stället på [MicroG](https://microg.org/), en app med öppen källkod som stödjer många av samma funktioner. För platstjänster kan man välja att använda mellan Apples och Mozillas platstjänster, eller helt förlita sig på GPS och en lokal databas som byggs upp gradvis. För pushnotiser krävs dock att man loggar in via ett Google-konto.

##### Appar på en "av-googlad" android?
Många appar förlitar sig som sagt på Googles proprietära tjänster för att fungera. I de flesta fall är den enklaste och mest bekväma lösningen att hitta alternativ som inte gör det. Dessutom kan det verka svårt att över huvud taget installera appar på en telefon utan Google Play Store.

En del av lösningen är [F-Droid](https://f-droid.org/), en appbutik som bara har appar med öppen källkod, men som visserligen har ganska skralt utbud. Detta kan hjälpas av att lägga till extra programvuralager, t.ex. [IzzyOnDroid](https://apt.izzysoft.de/fdroid/), men de flesta kommer fortfarande inte kunna få tag på alla appar de letar efter.

Förstås kan det vara så att apputvecklaren själv tillhandahåller en .apk-fil som går att installera på sin telefon, men det är sällan, det är krångligt, och de apparna uppdateras i regel inte automatiskt. Då är [Aurora Store](https://aurorastore.org/) det bästa alternativet, som tillåter att ladda ner appar anonymt eller med inlogg, om du behöver en app som du betalt för. 

Om dina appar inte går att hämta från Aurora Store, kanske de är tillgängliga via
nedladdning från webben, Github, APKPure, eller Huawei Appgallery. I så fall kan
du använda [Obtainium](https://github.com/ImranR98/Obtainium?tab=readme-ov-file#installation)
för att förenkla installations- och uppdateringsprocessen.

I värsta fall kan även EFF:s [APKeep](https://github.com/EFForg/apkeep) också vara ett alternativ. Den stödjer nedladdning från såväl Google, som F-Droid, APKPure och Huawei Appgallery. De sista två bör användas med eftertanke, i APKPure:s fall för deras modell inte har några säkerhetsgarantier, vilket lett till att de [haft problem med malware tidigare](https://news.yahoo.com/apkpure-app-contained-malicious-adware-182528292.html), och i Huawei Appgallery:s fall för att de har täta kontakter med den kinesiska staten, känd för sin övervakning.
