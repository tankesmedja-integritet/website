---
title: LineageOS
---

## LineageOS
---
LineageOS tar bort det mesta av Googles proprietära kod, utan att ersätta det med något annat. Detta leder till att många appar inte kommer fungera om man inte installerar Googles Android-appar eller ett alternativ till dessa. Dessutom stödjer lineageos inte låsta bootloaders, vilket försvagar säkerhetsmodellen.

LineageOS:s stora styrka är dess breda stöd för hårdvara. Din befintliga telefon har nästan garanterat stöd från LineageOS. Dessutom har LineageOS funnits ganska länge, och dess direkta företrädare, Cyanogenmod, var en av de första alternativa Android-varianterna.

- Stödjer **inte** verifierad boot
- Har **ingen kompatibilitet** med appar som förlitar sig på Google Play Services
