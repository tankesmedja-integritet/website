---
title: Firefox
---

## Firefox
---

### Inställningar

- Enhanced Tracking Protection: Strict
- Permissions
- Firefox Data Collection and Use
- HTTPS in all windows
- [DNS](../../dns)

### Tillägg

- Ublock Origin
