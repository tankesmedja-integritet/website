---
title: Webbläsare
---

## Webbläsare
---
Valet av webbläsare är ett av de viktigaste för att förhindra spårning online, en webbläsare har tillgång till allt du gör genom den, vilket många gånger är extremt privat, och kan antingen använda denna tillgång för att skydda dig från andras försök att övervaka, eller själviskt utnyttja den för att övervaka dig ännu mer.

#### [Firefox](./firefox)
Firefox är ett bra val av webbläsare ur integritetssynpunkt, de användardata som samlas in är lätta att välja bort, och med rätt inställningar kan den ge förstklassigt skydd mot spårning på nätet. Klicka på rubriken för att se inställningarna som bör justeras.

#### [Brave](./brave)
Brave är en relativt ny webbläsare som har ett explicit integritetsfokus. Den är baserad på Chromium, men har många funktioner och förändringar som gör att den är bland de mest integritetsrespekterande webbsidorna som finns. Den finansieras genom sin integrerade kryptovaluta, något som många förståeligen ryggar tillbaka för, men som går lätt att stänga av.

#### Safari
Safari tjänar mycket på att vara den förinstallerade webbläsaren på Apples produkter, och även om den inte aktivt spårar dig som Chrome gör, skyddar den dig inte särskilt effektivt från spårningen man utsätts för som vanlig internetanvändare.

#### [Google Chrome](./chrome)
Den mest använda webbläsaren är Google Chrome, och är en central del av Googles övervakningsmaskin. Den ger inget skydd mot spårning, och bidrar istället med ännu mer övervakning.

#### [Microsoft Edge](./edge)
Microsoft övergav sin förra webbläsare, Internet Explorer, som dominerade webbläsarmarknaden förra decenniet, och gick över till en chromium-baserad variant, som heter Edge. Den har minimala integritetsskydd och likt Chrome övervakar användaren själv.

#### Librewolf
Librewolf är en modifierad version av Firefox med utmärkt skydd för integriteten, men den är ideellt utvecklad och har ibland legat bakom med säkerhetsuppdateringar. Dessutom uppdaterar den inte sig själv automatiskt, vilket blir ett problem i längden om man använder vissa operativsystem (t.ex. Windows eller MacOS)

#### Tor browser
Tor är den absolut mest integritetsskyddande webbläsare som finns. Den använder Tor-nätverket som från början utvecklades av amerikanska myndigheter för att kryptera kommunikationen i flera lager. Den är byggd på Firefox och har många förändringar som å ena sidan gör den mycket mer integritetsskyddande, men också väldigt omständig att använda som sin vardagliga webbläsare.

#### Mullvad
Mullvad har släppt en webbläsare som är baserad på Tor-webbläsaren, men utan att använda Tor-nätverket. Den har många av de funktioner som stärker integritetsskyddet, men är av samma anledningar omständig att använda. Den är gratis att använda och man behöver inte vara användare av Mullvad VPN.

#### Källor och vidare läsning
[Privacy Guides om webbläsare](https://www.privacyguides.org/en/desktop-browsers/)

[Privacy Tools om webbläsare](https://www.privacytools.io/private-browser)

[privacytests.org](https://privacytests.org/)
!!! Privacytests.org utvecklas av en person som arbetar åt Brave, sedan metodiken kritiserats för att premiera just denna webbläsare har About-sidan uppdaterats för att göra situationen tydligare.
