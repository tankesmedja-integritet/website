---
title: Chrome
---

## Chrome
---

### Inställningar

- Permissions
- Telemetry
- HTTPS överallt
- Tredjepartskakor

### Tillägg
Måste logga in för att kunna istallera

- Ublock Origin


- Överväg istället [Ungoogled-chromium](https://github.com/ungoogled-software/ungoogled-chromium)
