---
title: Safari
---

## Safari
---

- Safari &rarr; Inställningar &rarr; Integritet
 - Förhindra spårning mellan webbplatser
- Safari &rarr; Inställningar &rarr; Avancerat
 - Använd avancerad spårnings- och fingeravtrycksskydd (?): all webbsurfning(?)

- DNS:
  - Systeminställningar &rarr; Nätverk
  - [DNS-servrar](../../dns)
Tillägg:
  - En reklamblockerare, såsom [AdGuard for Safari](https://apps.apple.com/us/app/adguard-for-safari/id1440147259?mt=12) eller [Ka-Block!](https://apps.apple.com/us/app/ka-block/id1335413823)

