---
title: Edge
---

## Microsoft Edge
---

### Inställningar
Klistra in ``edge://flags#dns-over-https`` i ditt addressfält, och tryck enter.
Sätt på säker DNS med hjälp av menyn.

Inställningar > Integritet, sök och tjänster (?) > Säkerhet
Byt till en integritetsbevarande
[DNS-tjänst](../dns)
