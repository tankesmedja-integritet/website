---
title:Kryptovalutor
---

För den som vill göra affärer anonymt är den enklaste lösningen oftast kontanter, men i vissa situationer kan elektronisk överföring vara att föredra.

Det finns många kryptovalutor som marknadsför sig som integritetsbevarande, men den vi skulle rekommendeframför allt är Monero, som funnits länge, används brett på Dark Web (på gott och ont, men det är ju ett tecken på att det finns stort förtroende), och vars kryptografiska funktioner generellt betraktas som vettiga.

För opartiskhetens skull finns även dessa anonyma kryptovalutor som alternativ:

- Zcash
- Verge
- Piratechain

Bitcoin är däremot *inte* anonymt, utan snarare *pseudonymt*, och alla transaktioner som syns i dess blockkedja är föremål för analys. Om du måste använda Bitcoin finns det vissa funktioner, som Lightning Network, som kan hjälpa något.
