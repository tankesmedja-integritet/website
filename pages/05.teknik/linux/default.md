---
title: Linux
---

## Linux
---

Det finns väldigt många varianter av Linux-baserade operativsystem, och det finns väldigt starka åsikter om vilket som är bäst. De skiljer sig också väldigt mycket från varandra, men med väldigt få undantag är även de med svagare integritetsskydd mycket bättre ur integritetssynpunkt än både Windows och MacOS.

Olika distributioner har olika för- och nackdelar, och fyller olika nischer.
Följande distributioner är inriktade på nybörjare, och respekterar din integritet
direkt eller med minimala justeringar:

- [Fedora](http://fedoraproject.org)
- [Ubuntu](https://ubuntu.com/download)

[PrivacyGuides](https://www.privacyguides.org/en/desktop)
