---
title: Teknik
body_classes: 'title-center title-h1h2'
content:
    items: '@self.children'
    limit: 5
    order:
        by: date
        dir: desc
    pagination: true
visible: false
metadata:
    refresh: 30
    description: 'Stärk din integritet med våra enkla, nybörjarvänliga guider'
    keywords: 'integritet, privacy, svenska, swedish, guide, tech'
---

# Teknik

---

I sidan för varje rekommenderad (eller ej rekommenderad) tjänst finns mer information ur integritetssynpunkt, samt eventuella inställningar som kan eller bör ändras.

Materialet som finns här idag förlitar sig på diverse källor, som finns angivna i respektive artikel eller längst ner på denna sida.



!> Onlinetjänster

### [Sökmotorer](./soekmotorer)
Undvik:
!!!! - [Google](./soekmotorer#google)
!!!! - [Bing](./soekmotorer#bing)
!!!! - [Yahoo!](./soekmotorer#yahoo!)

Använd hellre:
! - [DuckDuckGo](./soekmotorer#duckduckgo)
! - [Startpage](./soekmotorer#startpage)
! - [Brave Search](./soekmotorer#brave_search)
! - [SearXNG](./soekmotorer#searxng)

### [E-post](./epost)
Undvik:
!!!!  - [Gmail](./epost#gmail)
!!!!  - [Outlook](./epost#microsoft-outlook)

Använd hellre:
! - [Protonmail](./epost#protonmail)
! - [Tutanota](./epost#tutanota) 
! - [Startmail](./epost#startmail)
! - [Mailbox.org](./epost#mailbox-org)

### [Moln/Drivetjänster](./moln)
Undvik:
!!!! - [Google](./moln#google)
!!!! - [Microsoft](./moln#microsoft)
!!!! - [Apple](./moln#apple)

Använd hellre:
! - Cryptpad (1 GB gräns för gratisanvändare)  
! - Protondrive
! - Nextcloud (självhostad filserver)          
! - OnlyOffice (självhostad online-office-svit)

### [Sociala medier](./sociala-medier)

### [Chattar](./chattar)
! - Signal  
! - Briar   
! - Element 
! - Session 
!!! - Telegram?
!!! - Whatsapp?

### [Media/Streaming](./streaming)
! - Bandcamp       
! - gog.com        
! - Fysiska kopior 

### Lösenordshanterare
! - Bitwarden 
! - KeePassXC 

### [Mjukvarusviter](./sviter)

### [Hälsa](./haelsa)

!@

!> Webbläsare

#### [För datorer](./webblaesare)
Undvik: 
!!!! - [Google Chrome](./webblaesare/chrome)
!!!! - [Microsoft Edge](./webblaesare/edge)

Använd hellre:
! - [Firefox](./webblaesare/firefox)
! - [Brave](./webblaesare/brave)

#### [För Android](./android/webblaesare)

Undvik:
!!!! - Chrome

Använd hellre:
! - Brave
! - Cromite
! - DuckDuckGo
! - Firefox Focus

#### För IOS
I nuläget finns inga meningsfulla alternativ till Safari på IOS

!@


!> Dator-operativsystem
De tre vanligaste operativsystemen för datorer är idag
- [Windows](./windows)
- [MacOS](./macos)
- [Linux](./linux)

Av dessa är Windows nästan alltid sämst ur integiretetssynpunkt. För varje ny version samlar Microsoft in mer och mer användardata. Det finns inställningar och diverse hacks som kan förbättra situationen något, men med tanke på hur omständigt det är, och hur stor risk det är att saker går sönder, är det ett mycket bättre att byta till ett annat operativsystem.

MacOS är något bättre, och med rätt inställningar kan det vara ett hyggligt integritetsbevarande operativsystem.

Olika Linuxbaserade operativsystem, eller *distributioner*, kan variera mycket i hur integritetsbevarande de är. T.ex. är TAILS och QUBES två linuxbaserade operativsystem som har ett extremt fokus på integritet, men även Googles [ChromeOS](https://wikipedia.org/ChromeOS) och Nordkoreas [Red Star OS](https://wikipedia.org/Red_Star_OS) är linuxbaserade, och har extremt mycket övervakning. Sammantaget är de flesta populära linuxdistributioner bättre alternativ än både Windows och MacOS, men variationen är som sagt stor.
!@

!> Smarta telefoner

I dagsläget finns tre alternativ vad gäller telefoner:

- En telefon med förinstallerat [Android](./android)
- En [Iphone](./ios)
- En telefon med ett egeninstallerat [Android](./android)

Av dessa är det sämsta en "vanlig" Android-telefon, inte bara är Googles hela affärsmodell [byggd på övervakning](./oevervakning-som-affaersmodell), det är stor risk att även telefonens tillverkare samlar in information.

Iphone är något bättre ur integritetssynpunkt, åtminstone om man undviker Icloud och ser till att välja rätt inställningar för telefon och Apple-konto. Eftersom Ios bara finns som alternativ för Apples egna telefoner blir man dock något begränsad i utbud, och även om det är bättre än de flesta Android-telefoner finns många brister.

Det bästa alternativet är att installera en integritetsfokuserad Andoid-version, de finns listade i [artikeln om Android](./android), vissa av dessa har dock begränsat hårdvarustöd och andra har begränsad kompatibilitet.
!@

!> Övrigt, inklusive specifika tips för de med förhöjd hotbild

- [Internet of Things-apparater och Smarta hem](./iot-smarta-hem)

- [Utsatt för riktad spårning/stalking?](./sparning)
- [VPN och Tor](./vpn)
- [Kryptovalutor](./kryptovalutor)
- [Avancerade operativsystem och BIOS](./avancerade-os-bios)
!@

!> Källor, vänner, resurser, och vidare läsning:
- [Surveillance Self-defence från Electronic Frontier Foundation](https://ssd.eff.org         )
- [Your Online Choices](https://youronlinechoices.eu) från industrigruppen European Interactive Digital Advertising Alliance
- [privacytools.io](https://privacytools.io     )
- [privacyguides.org](https://privacyguides.org   )
- [justdelete.me](https://justdelete.me       ) har guider för hur man raderar konton hos olika tjänster
- [Nikka Systems](https://nikkasystems.com), en IT-säkerhetskonsultfirma grundad av Karl Emil Nikka, som har poddar och artiklar om IT-säkerhet
- [Privacy Not Included](https://foundation.mozilla.org/en/privacynotincluded/), en konsumentguide från Mozilla
- [Terms Of Service; Didn't Read](https://tosdr.org)
- [Privacy International](https://privacyinternational.org/)
!@
