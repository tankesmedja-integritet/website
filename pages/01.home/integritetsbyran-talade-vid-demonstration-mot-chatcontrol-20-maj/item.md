---
title: 'Integritetsbyrån talade vid demonstration mot chatcontrol 20 maj'
published: true
date: '22-05-2023 20:14'
---

Den 20 maj talade Peter Hellman från Integritetsbyrån vid en demonstration mot chatcontrol på Mynttorget i Stockholm som ca 200 personer deltog i. Här hittar du hela talet. 

Mer om demonstrationen och om chatcontrol finns på [https://chatcontrol.se](https://chatcontrol.se) och i det här inslaget från [Sveriges Radio](https://sverigesradio.se/artikel/eu-kontrollera-digitala-meddelanden-hundratals-demonstranter-i-stockholm).

> Chatcontrol, är – missta er inte – massiv övervakning av privat kommunikation 
> Chatcontrol handlar om att övervaka kommunikationen mellan alla de miljontals människor som har rent mjöl i påsen – för att kanske hitta ett litet fåtal brottslingar. Det hör inte hemma i vår demokrati.

**Peter Hellmans tal i sin helhet**

"Jag har förmånen att få tala här som ordförande i den precis nystartade tankesmedjan Integritetsbyrån som kommer lanseras senare i sommar. Vi kommer att vara ett forum för kritisk debatt och diskussion i integritetsfrågor. 

Jag arbetar också till vardags som advokat. Som ledamot av advokatsamfundet man ett särskilt ansvar för att värna grundläggande fri- och rättigheter. Även med det perspektivet står jag här idag. En av dessa är rätten till och skyddet för privatlivet – som finns i Europakonventionen och i vår grundlag. 

Rätten till ett privatliv och rätten till personlig integritet förutsätter att man kan kommunicera för att dela och utbyta tankar, åsikter, kritik och idéer utan att övervakas av staten och det allmänna. 

”Har man bara rent mjöl i påsen finns det väl inget att oroa sig för?” Det argumentet hör vi gång efter annan. Men skyddet för den personliga integriteten är inte bara frågan om vad man som individ har i sin påse eller inte. Integritet är också ett samhällsintresse. Personlig integritet och rätten till privat kommunikation är ett villkor för allas fria och kritiska deltagande i en aktiv samhällsdebatt, för att enskilda ska kunna agera och framföra synpunkter på samhället, vilket är kärnan i vår demokrati.

För att debatten ska hämmas räcker det med insikten om att det genom Chatcontrol finns enn massövervakningsapparat som har möjlighet att övervaka och spåra oss. Det riskerar att leda till självcensur; att man inte längre vågar fritt utbyta tankar och idéer. Själva demokratin förutsätter att människor inte upplever sig övervakade och registrerade. 

Chatcontrol, är – missta er inte – massiv övervakning av privat kommunikation 
Chatcontrol handlar om att övervaka kommunikationen mellan alla de miljontals människor som har rent mjöl i påsen – för att tråla fram ett litet fåtal brottslingar. Det hör inte hemma i vår demokrati.

Det är lätt att se hur ett system som chatcontrol skulle kunna missbrukas. Hur data läcker. Hur avkrypterad information kommer i orätta händer. Vi har dessutom gång på sett exempel på ändamålsglidningar; när staten och polisen får verktyg som var tänkt att användas för någonting annat, men där man succesivt utökar räckvidden och urholkar skälen för övervakning.

Polisen gör misstag. Information feltolkas. Vi ser det gång på gång. Informationen som granskas via chatcontrol kommer misstolkas. Oskyldiga far- och morföräldrar kommer få se polisen knackandes på deras dörrar för att de fått bilder på sina barnbarn. Polisen kommer knacka på fler dörrar, men på fel dörrar. För chatcontrol kommer inte leda till det angivna ändamålet.

Vi ser också antidemokratiska vindar gripa för sig i Europa. Att då i det läget ge staten tillgång till ett massivt övervakningsverktyg som kan gå igenom all elektronisk kommunikation är rent utsagt vansinne. 

Det är därför genom att säga nej till massövervakning och värdesätta integritet som vi tillsammans bygger det starka, fria, demokratiska samhället."

(Mindre avvikelser från det skrivna talet kan ha förekommit)

_Övriga talare var:_

* Jon Karlung, VD, Bahnhof
* Ann-Therese Enarsson, VD, Tankesmedjan Futurion
* Andrea Andersson-Tay, riksdagsledamot, Vänsterpartiet
* Anton Holmlund, vice ordförande, Liberala Ungdomsförbundet
* Fabian Lidbaum, språkrör, Grön Ungdom Stockholm
* Katarina Stensson, partiledare, Piratpartiet
* Mikael Blomstrand, styrelseledamot, Centerstudenter
* Mattias Axell, ordförande, DFRI (arrangör)
* Samuel Skånberg, Kamratdataföreningen Konstellationen (arrangör)
* Henrik Alexandersson, 5 juli-stiftelsen (arrangör, konferencier)

Foto: Zee Vieira med licens [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).