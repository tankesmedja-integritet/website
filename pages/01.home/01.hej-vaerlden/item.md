---
title: 'Välkommen till tankesmedjan för integritetsfrågor!'
published: true
date: '28-03-2023 07:18'
visible: false
---

Den 27 mars grundades tankesmedjan formellt, våra första [stadgar](/02.about/stadgar) antogs, och den första styrelsen valdes. Styrelsen roll är att samordna föreningens verksamhet och att bjuda in intressanta opinionsbildare och debattörer att skriva för tankesmedjan. 

Tankesmedjans mål är att ge människor starkare kontroll över sin integritet och stärkt skydd av sina rättigheter, bland annat gennom att verka för att öka kunskapen hos Sveriges befolkning i integritetsfrågor, genom att bidra till att fördjupa och nyansera debatten i olika forum i frågor där integritetsaspekten är av relevans.

Vi ser fram emot att jobba tillsammans för att stärka allas vår integritet!

![styrelsen](styrelsen.jpeg "styrelsen")
På bilden, från vänster: Peter Hellman, Vladan Lausevic, Philip Johansson och Katarina Stensson.


