---
title: 'Drönarövervakning av demonstrationer - ett demokratiproblem?'
media_order: FuyOFSXXsAsLSmI.jpeg
visible: false
date: '01-05-2023 17:52'
---

I fredags informerade [Polismyndigheten](https://polisen.se/aktuellt/nyheter/2023/april/kamerabevakning-med-uas-mandagen-den-1-maj/) om att stora delar av Stockholms innerstad skall kameraövervakas med drönare på första maj. Denna typ av massövervakning av åsiktsyttringar riskerar i förlängningen att utgöra ett demokratiproblem.

Första maj har sedan 1890 varit en demonstrationsdag för framförallt arbetarrörelsen och dagen har ända sedan 1938 varit allmän helgdag i Sverige. Under mer än 100 år har demonstrationer hållits denna dag. Demonstrationsrätten på första maj har därmed blivit en viktig del av den svenska demokratin. 

Polisen har till uppgift att förebygga och beivra brott. Vän av ordning kanske invänder att användningen av drönare ger polisen möjlighet att snabbt få en överblick över de olika demonstrationstågen för att bättre kunna fördela resurser under dagen – och det är givetvis helt riktigt. Men övervakningen har också en tydlig baksida.

Den senaste tiden har statens möjligheter till och kapacitet för övervakning av enskilda ökat exponentiellt. Samtidigt som övervakningen ökat har möjligheten till anonym kommunikation och oövervakade åsiktsyttringar minskat. Ett exempel på detta är att det numera inte går att skaffa anonyma kontantkort för att anonymt kunna visselblåsa eller förmedla andra uppgifter om missförhållande och samhällsproblem mellan enskilda eller till medier. 

I dagarna har vi också kunnat läsa hur den nya lagen om utlandsspioneri lett till att SVT censurerat sig själva av rädsla för repressalier om man sänt ett inslag med uppgifter om Ukrainas energiförsörjning, härrörande från den så kallade Pentagonläckan. Samtidigt finns en bred enighet om att nu införa lagstiftning som möjliggör avlyssning av enskilda utan konkret brottsmisstanke. 

Sammantaget krymper utrymmet för den fria åsiktsbildningen där rädsla för övervakning och repressalier redan sätter tydliga spår. Genom polisiär övervakning riskerar man att skapa samma typ av rädsla även för medverkan i demonstrationer. 

Den fria åsiktsbildningen kräver att enskilda kan demonstrera, agera och fritt utbyta tankar, idéer och åsikter. Detta gäller särskilt åsikter som i politiskt oroliga tider eller tider av stark polarisering riskerar att utsätta opinionsbildarna för andras missaktning.

Advokatsamfundet har vid flera tillfällen pekat på att enbart insikten om att det finns en möjlighet att övervaka och spåra enskilda individer riskerar att få ytterst menlig inverkan på utövandet av en mängd demokratiska fri- och rättigheter. 

Vill man skydda den fria åsiktsbildningen bör man därför vara mycket försiktig med att använda för övervakning som lätt skulle kunna missbrukas. Vi vet att det finns en rad exempel där staten tidigare upprättat olagliga eller olämpliga register – från IB-affären till det så kallade Romregistret, där människor registrerats för politiska uppfattningar eller släktskap inom en viss etnisk gruppering trots att registreringen var förbjuden. Missbruk av de verktyg polisen tilldelas har visat sig ske gång på gång. 
Det räcker därför inte med lagstiftning som förbjuder viss registrering eller behandling av insamlade data – historiens exempel ger ändå skäl för en inte obefogad oro för att något liknande skall inträffa igen. Även om det i regeringsformen finns ett förbud mot registrering på grund av politisk åskådning måste medborgarna kunna lita på att det efterföljs. Därför måste polisen förbjudas att använda drönare och andra former av kamerabevakning vid politiska demonstrationer, i vart fall innan någon konkret brottslighet uppdagats som föranleder ett ingripande. Även när ett ingripande är nödvändigt måste övervakningen begränsas till den eller de som begår brotten och inte täcka övriga, fredliga, demonstranter.

Statlig övervakning utgör alltid ett intrång i de rättsstatliga och demokratiska värden som den personliga integriteten ska skydda. Att människor som vill uttrycka sina åsikter upplever att det finns en risk för att de är övervakade eller registrerade riskerar att succesivt hämma den fria åsiktsbildningen. I exemplet med SVTs självcensur har vi redan sett det hända. 

_Peter Hellman_

Ursprungligen publicerad på integritetsbyran.se den 1 maj. Reviderad version publicerad på [altinget.se](https://www.altinget.se/artikel/forbjud-polisen-att-anvanda-dronare-vid-demonstrationer) den 10 maj.
