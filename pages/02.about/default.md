---
title: Integritetsbyrån
body_classes: 'title-center title-h1h2'
---

# Integritetsbyrån
## tankesmedjan för integritets- och privatlivsfrågor

_Ingen får utsättas för godtyckligt ingripande i fråga om privatliv, familj, hem eller korrespondens och inte heller för angrepp på sin heder eller sitt anseende. Var och en har rätt till lagens skydd mot sådana ingripanden och angrepp._

\- FN:s deklaration om mänskliga rättigheter, punkt 12.  

_Var och en är gentemot det allmänna skyddad mot påtvingat kroppsligt ingrepp även i andra fall än som avses i 4 och 5 §§. Var och en är dessutom skyddad mot kroppsvisitation, husrannsakan och liknande intrång samt mot undersökning av brev eller annan förtrolig försändelse och mot hemlig avlyssning eller upptagning av telefonsamtal eller annat förtroligt meddelande._

_Utöver vad som föreskrivs i första stycket är var och en gentemot det allmänna skyddad mot betydande intrång i den personliga integriteten, om det sker utan samtycke och innebär övervakning eller kartläggning av den enskildes personliga förhållanden_  

\- Regeringsformen 6§ 2. kap

Människor är oroliga för sin personliga integritet, men upplever att de inte kan påverka sin situation eller hur och när den kränks ([IMY:s integritetsrapport 2019](https://www.imy.se/publikationer/nationell-integritetsrapport-2019/)).

#### Mål & syfte  

Tankesmedjans **_mål_** är att ge människor starkare kontroll över sin integritet och stärkt skydd av sina rättigheter.  

Vidare är tankesmedjans **_syfte_** att verka för att  

*   öka kunskapen hos Sveriges befolkning i integritetsfrågor, genom att bidra till att fördjupa och nyansera debatten i olika forum i frågor där integritetsaspekten är av relevans.
    
*   påverka politiken att agera i linje med regeringsformen 6§ kap 2, europakonventionen artikel 8 och FNs deklaration om mänskliga rättigheter punkt 12, som de är citerade ovan.  
    

#### Målgrupp  

Målgruppen för tankesmedjans arbete är Sveriges befolkning och svenska makthavare i Sverige och EU. Vårt huvudsakliga arbetsspråk är svenska. Detta eftersom vi bedömer att det redan finns ett antal framgångsrika organisationer med liknande syfte och målsättningar som agerar på internationell nivå eller med engelska som huvudsakligt arbetsspråk. Dessa kan dock utgöra samarbetspartners.  

  

### Värderingar  

Tankesmedjan tar inte partipolitisk ställning, och en bredd av åsikter och diskussioner om avvägningar mellan rätten till integritet och andra rättigheter och möjligheter måste få plats under tankesmedjans tak. Den grundläggande värdering som de aktiva inom tankesmedjan delar är att integritetsfrågan är viktig och behöver en både djupare och mer nyanserad behandling inom samhällsdebatten.

### Vad vi vill göra

#### Folkbildning  

Vår första och viktigaste uppgift är att höja allmänhetens kunskapsnivå i frågor som rör personlig integritet.

Kunskap ska naturligtvis kommuniceras faktagranskad och väl underbyggd, men lika viktigt är att den är _lättillgänglig_. Målgruppen ska förstå frågan, hur den påverkar dem, och vad de kan göra för att stärka sin egen och andras integritet.  

Detta sker initialt genom att samla kunskap och debatt i ett kunskapscentrum online, i form av en hemsida och/eller nyhetsbrev, som utrustar läsaren med juridisk, teknisk och politisk kunskap om integritetsfrågor. Detta kan kombineras med taktik för att att nå ut till relevanta målgrupper.  

Tankesmedjan kommer vara en samlingspunkt för människor med driv och spetskompetens. På sikt kan vi därigenom utveckla möjligheten att ta fram ny kunskap på området, i form av rapporter, böcker och seminarieserier.

#### Politisk påverkan  

Vår andra huvuduppgift är att påverka politiken och juridiken, genom att driva debatt i frågor som rör personlig integritet, lyfta och koordinera röster som driver i samma riktning, och använda det nätverk och den kunskap vi samlar för att uppmärksamma pågående och kommande lagstiftning och påverka dess riktning och utfall.  

Påverkan kan ske i såväl sociala medier som traditionella och i andra forum. Exempel på format i det initala stadiet kan vara tidningarnas debattsidor, medverkan i poddar, Twitterspaces, och samarbete för att öka synlighet och diskussion i integritetsfrågor i sociala medier. Tankesmedjan kan återpublicera och sammanfatta nätverkets olika bidrag i egna kanaler (sociala medier, nyhetsbrev, hemsida mm.)  

#### Nätverk & kunskapsdelning  

Inom tankesmedjan bör vi även dra nytta av vårt gemensamma intresse och olika kompetenser, för att mötas, diskutera, lära, stötta och inspireras av varandra, genom närverksträffar och andra samarbetsformer för att dela kunskap och idéer.

##   

### Vad vi inte är

Ett politiskt parti - Vi behandlar inte frågor som ligger utanför integritetsområdet.

Teknikstartup - Vi tar inte fram egna tekniska verktyg

Compliancekonsulter - Vi kommer inte hjälpa företag eller organisationer "GDPR-säkra" sig.


  

### Det praktiska

Tankesmedjan kommer möjliggöra två huvudsakliga kategorier av engagemang:

Koordinerande grupp: En mindre grupp personer som gör kontinuerligt operativt arbete för att driva tankesmedjan framåt. Exempelvis sätta upp och utveckla hemsida, sociala medier och interna kanaler och verktyg för tankesmedjan, hitta och söka finansiering, inleda samarbeten och vara drivande i att arrangera träffar och event.

Debattörer: En stor grupp som bidrar till att lyfta integritetsfrågan på något sätt; genom att skriva debattartiklar och andra texter, delta i eller driva poddar, utbilda, aktivt delta på nätverksträffar eller på annat sätt lyfta integritetsfrågan i samhället och bidra till att tankesmedjans syfte kan uppfyllas. Det finns inget krav på storleken av bidraget, och engagemangsnivån kan variera mellan olika medlemmar och med tiden.  

#### Juridisk form och styrning  

Tankesmedjan kommer drivas i form av en ideell förening. En styrelse kommer väljas av medlemmarna. Styrelsen har det yttersta ekonomiska och juridiska ansvaret för tankesmedjan, samt beslutar om strategisk riktning. Styrelsen beslutar även om medlemskap. Styrelsen och den koordinerande gruppen kan delvis överlappa, men den koordinerande gruppen är i högre grad flexibel och flytande.  

  

#### Finansiering  

På längre sikt krävs en plan för finansiering. Vi ser framför oss en kombination av

*   Frivilliga donationer
    
*   Transparent sponsring från vissa företag och aktörer som jobbar mot samma syfte
    
*   Dra nytta av den kompetens vi har inom nätverket för att erbjuda och ta betalt för exempelvis utbildningar, deltagande i expertpaneler och likn.
    
*   Söka stiftelsepengar och andra bidrag.

### Formalia
- [Stadgar](./stadgar)

### Kontaktpersoner
- Peter Hellman
- [Katarina Stensson](mailto:katarina.stensson@piratpartiet.se)
- [Philip Johansson](mailto:philip.johansson@piratpartiet.se)
- [Vladan Lausevic](mailto:vladlau89@gmail.com)
