---
title: Stadgar
---
## Stadgar för den ideella föreningen *Integritetsbyrån – tankesmedjan för integritets- och privatlivsfrågor*

*Antagna vid konstituerande årsmöte 2023-03-27*
#### § 1 Namn och ändamål m.m.
Föreningens namn är Integritetsbyrån.

Föreningens ändamål är att verka folkbildande för att öka kunskapen om integritetsfrågor i syfte att främja att människor skall få starkare skydd för sin integritet och stärkt skydd för sina rättigheter enligt grundlag, Europakonvention och andra internationella konventioner. Föreningens skall även verka opinionsbildande och bedriva påverkansarbete för att stärka integriteten och skyddet för privatlivet.

Föreningen är partipolitiskt och religiöst obunden.

#### § 2 Säte
Föreningens styrelse har sitt säte i Stockholms kommun.

#### § 3 Medlemskap
Till medlem i föreningen kan antas den som åtar sig att verka för föreningens ändamål. Medlemskap uppnås efter erlagd medlemsavgift och beslut av föreningens styrelse.

Ansökan om medlemskap görs skriftligen hos styrelsen.

#### § 4 Medlemsavgift
Medlemsavgift erläggs för kalenderår och med det belopp och vid den tidpunkt som föreningsstämman eller styrelsen beslutar.

#### § 5 Föreningsstämma
Föreningsstämman är föreningens högsta beslutande organ.

*Digital föreningsstämma*

Föreningsstämma kan hållas helt eller delvis digitalt efter styrelsens beslut därom. Om föreningsstämman skall hållas helt digitalt ska kallelsen innehålla uppgift om hur medlemmarna skall gå till väga för att delta och för att rösta. 

*Ordinarie föreningsstämma*

Ordinarie föreningsstämma skall hållas årligen före april månads utgång. 

*Extra föreningsstämma*

Extra föreningsstämma kan hållas när styrelsen sammankallar till sådan eller när minst hälften av föreningens medlemmar eller föreningens revisorer hos styrelsen så begär.

*Kallelse*

Styrelsen skall skriftligen kalla till föreningsstämma och kallelse skall ske senast 4 veckor före stämman. Kallelsen skickas till den post- eller e-postadress som medlemmarna anmält till styrelsen.

*Rösträtt och omröstning*

Medlem som erlagt fastställd medlemsavgift har rösträtt på föreningsstämman.

Beslut fattas med enkel majoritet. För stadgeändring krävs dock 2/3 majoritet. Vid lika röstetal har föreningsstämmans ordförande utslagsröst, utom vid val då lotten istället skall avgöra.

#### § 6 Ärenden på föreningsstämma 

På ordinarie föreningsstämma skall följande ärenden behandlas: 
1. Föreningsstämmans öppnande 
1. Fastställande av röstlängd 
1. Val av stämmoordförande, protokollförare och två justeringspersoner 
1. Fråga om mötets stadgeenliga utlysande 
1. Fastställande av dagordning 
1. Föredragning av styrelsens verksamhetsberättelse 
1. Fastställande av balans- och resultaträkning 
1. Föredragning av revisionsberättelse  
1. Fråga om ansvarsfrihet för styrelsen 
1. Fastställande av medlemsavgifter och senaste dag för betalning 
1. Fastställande av styrelsens storlek  
1. Val av ordförande 
1. Val av övriga styrelseledamöter 
1. Val av revisor(er) 
1. Övriga frågor 
1. Föreningsstämmans avslutande 

Vid extra föreningsstämma behandlas de ärenden som anges i kallelsen. 

#### § 7 Styrelsen 
Det åligger styrelsen att ansvara för föreningens löpande förvaltning samt att verkställa av årsmötet fattade beslut, handha föreningens ekonomiska angelägenheter, föra löpande räkenskaper samt att besluta om inval av medlemmar i föreningen.  

Styrelsen skall bestå av en ordförande och ytterligare minst två och maximalt sex ledamöter.  

Styrelsen sammanträder på kallelse av ordförande. Styrelsen är beslutsmässig när minst hälften av ledamöterna deltar vid styrelsemöte. Vid lika röstetal har ordföranden utslagsröst.

Föreningens firma tecknas av styrelsen. Styrelsen kan dessutom inom sig utse särskilda firmatecknare som därigenom ges generell fullmakt att företräda föreningen.  

#### § 8 Räkenskapsår 
Föreningens räkenskapsår är kalenderår.  

#### § 9 Revision 
För granskning av styrelsens förvaltning och föreningens räkenskaper utses en eller två revisorer.  Revisionsberättelse skall överlämnas till styrelsen senast 14 dagar före ordinarie föreningsstämma. I revisionsberättelsen skall revisorerna tillstyrka eller avstyrka ansvarsfrihet för styrelsen. 

#### § 10 Uteslutning 
Medlem som bryter mot dessa stadgar, mot av styrelsen i behörig ordning fattade beslut, mot av föreningen ingångna avtal eller som motarbetar föreningens ändamål och intressen eller som vållar föreningen skada kan uteslutas av föreningsstämma. Samma sak gäller medlem som inte erlägger föreskriven medlemsavgift. I avvaktan på stämmobeslut får styrelsen tillfälligt stänga av en medlem, som skäligen befaras kunna vålla föreningens skada, från föreningens verksamhet. Detta gäller dock inte styrelseledamot eller revisor.  

#### § 11 Stadgeändring 
Stadgeändring beslutas på föreningsstämma. Ärende som rör stadgeändring får endast behandlas på föreningsstämma under förutsättning att förslaget lämnats till styrelsen senast två månader före stämman. Förslaget på stadgeändring skall skickas ut till samtliga medlemmar tillsammans med kallelse till den föreningsstämma där ärendet skall behandlas.  

#### § 12 Föreningens upplösning 
Beslut om föreningens upplösning kan endast ske om likalydande beslut därom fattas på två på varandra följande föreningsstämmor, mellan vilka minst tre månader förflutit och varav minst en är ordinarie. Besluten skall fattas med minst 2/3 majoritet på varje sådan stämma.  Vid föreningens upplösning fördelas föreningens tillgångar, sedan alla skulder betalats, i enlighet med föreningsstämmans beslut. 
