---
title: Svenska register
---
## Svenska register

- Ratsit, Eniro, Hitta.se
  - [Ta bort dig själv](https://www.reddit.com/r/sweden/comments/11mt7ku/l%C3%A4nkar_f%C3%B6r_att_bli_bortagen_p%C3%A5_n%C3%A4tet/)
- PKU-registret och andra biobanker
  - [Ta bort dig själv](https://biobanksverige.se/samtycke/nej-talong/)
- [Socialstyrelsens register](https://www.socialstyrelsen.se/statistik-och-data/register/)

