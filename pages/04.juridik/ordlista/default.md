---
title: Ordlista
---
## Ordlista

- Ändamålsglidning
> När ett register eller en befogenhet som införts med ett syfte, börjar användas för andra ändamål, t.ex. när PKU-registet använts för brottsbekämpning, eller när reglerna för datalagring breddats flera gånger

- Säkerhetsteater
> Åtgärder som känns som att de hjälper säkerheten, men i själva verket är meningslösa, t.ex. röntgenmaskiner på flygplatser
