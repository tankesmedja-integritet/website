---
title: Opinion
body_classes: 'title-center title-h1h2'
visible: false
published: true
---

# Opinion

[Kameraövervakning löser inte problemen](https://www.svd.se/a/gEEWoL/kameraovervakning-loser-inte-problemen-skriver-peter-hellman), Svenska Dagbladet den 14 oktober 2022

[Ingreppen i enskildas integritet går för långt](https://www.svd.se/a/JQxL46/peter-hellman-ingreppen-i-enskildas-integritet-gar-for-langt), Svenska Dagbladet den 9 oktober 2022

[Slösa inte pengar på mer verkningslös kameraövervakning](https://www.gp.se/debatt/slösa-inte-pengar-på-mer-verkningslös-kameraövervakning-1.79119093), Göteborgs-Posten den 21 augusti 2022

[Ökad hemlig avläsning hot mot integriteten](https://www.svd.se/okad-hemlig-avlasning-hot-mot-integriteten), Svenska Dagbladet den 20 april 2021

[Polisens övervakningsiver en risk för samhällets demokratiska värdegrund](https://www.dagensjuridik.se/nyheter/polisens-overvakningsiver-en-risk-for-samhallets-demokratiska-vardegrund/), Dagens Juridik den 30 mars 2021

[Stor risk för missbruk om ansiktsigenkänning tas i bruk](https://www.gp.se/debatt/stor-risk-för-missbruk-om-ansiktsigenkänning-tas-i-bruk-1.33281049), Göteborgs-Posten den 30 augusti 2020
